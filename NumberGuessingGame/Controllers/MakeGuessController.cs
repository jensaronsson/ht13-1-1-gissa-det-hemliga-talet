﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NumberGuessingGame.Models;

namespace NumberGuessingGame.Controllers
{
    public class MakeGuessController : Controller
    {
        //
        // GET: /MakeGuess/

        public ActionResult Index()
        {
            Session["SecretNumber"] = new SecretNumber();
            return View("Index");
        }

        [HttpPost]
        public ActionResult Index(GuessViewModel gm)
        {
            if (Session["SecretNumber"] == null)
            {
                return View("Index");
            }
            var secretNumber = (SecretNumber)Session["SecretNumber"];

            if (ModelState.IsValid)
            {
                secretNumber.MakeGuess(gm.Guess);
                Session["SecretNumber"] = secretNumber;
                return View("Index", secretNumber);
            }
            
            return View("Index", secretNumber);
        }
        
    }
}
