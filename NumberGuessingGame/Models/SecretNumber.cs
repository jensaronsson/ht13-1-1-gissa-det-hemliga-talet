﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NumberGuessingGame.Models
{
    public class SecretNumber
    {

        private int? _number;
        private List<GuessedNumber> _guessedNumbers;
        private GuessedNumber _lastGuessedNumber;
        public const int MaxNumberOfGuesses = 7;

        public bool CanMakeGuess
        {
            get 
            {
                if (Count >= MaxNumberOfGuesses || _lastGuessedNumber.Outcome == Outcome.Right)
                {
                    return false;
                }
                else
                {
                    return true;
                }
                
            }
       } 

        public int Count
        {
            get 
            {
                return _guessedNumbers.Count;
            }
        }

       
        public IList<GuessedNumber> GuessedNumbers
        {
            get
            {
                return _guessedNumbers.AsReadOnly();
            }

        }
        public GuessedNumber LastGuessedNumber
        {
            get
            {
                return _lastGuessedNumber;
            }
        }


        public int? Number
        {
            get
            {
                if (CanMakeGuess)
                {
                    return null;
                } 
                else 
                {
                    return _number;
                }
            }
            private set
            {
                _number = value;
            }
        }


        public Outcome MakeGuess(int guess)
        {
            _lastGuessedNumber.Number = guess;
            _lastGuessedNumber.Outcome = Outcome.Indefinite;

            if (guess > 100 || guess < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (CanMakeGuess)
            {
                var result = _guessedNumbers.Exists(item => item.Number == guess);
                if (result)
                {
                    return _lastGuessedNumber.Outcome = Outcome.OldGuess;
                }
                else
                {
                    if (guess > _number)
                    {
                        _lastGuessedNumber.Outcome = Outcome.High;
                    }
                    else if (guess < _number)
                    {
                        _lastGuessedNumber.Outcome = Outcome.Low;
                    }
                    else if (guess == _number)
                    {
                        _lastGuessedNumber.Outcome = Outcome.Right;
                    }

              
                }
            }
            else
            {
                return _lastGuessedNumber.Outcome = Outcome.NoMoreGuesses;
            }
            _guessedNumbers.Add(_lastGuessedNumber);
            return _lastGuessedNumber.Outcome;
        }
        


        public void Initialize()
        {
            _guessedNumbers    = new List<GuessedNumber>(MaxNumberOfGuesses);
            _lastGuessedNumber = new GuessedNumber();
        }

        public SecretNumber()
        {
            Initialize();
            Number = new Random().Next(1, 101);
        }

    }
}