﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NumberGuessingGame.Models
{
    public class GuessViewModel
    {
        [Required(ErrorMessage = "Du måste gissa på något")]
        [DisplayName("Gissa det hemliga talet mellan 1 - 100")]
        [Range(1, 100, ErrorMessage = "Du kan inte gissa på ett tal utanför talrymden 1 - 100")]
        public int Guess { get; set; }
    }
}